package com.rolnicza.spoldzielnia.repository;

import com.rolnicza.spoldzielnia.modele.Field;
import org.springframework.data.jpa.repository.JpaRepository;

public interface FieldRepository extends JpaRepository<Field,Integer> {
}
