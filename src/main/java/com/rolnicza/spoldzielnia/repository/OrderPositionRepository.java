package com.rolnicza.spoldzielnia.repository;

import org.springframework.data.jpa.repository.JpaRepository;

public interface OrderPositionRepository extends JpaRepository<OrderRepository,Integer> {
}
